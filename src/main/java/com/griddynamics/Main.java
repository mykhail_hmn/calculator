package com.griddynamics;


import java.util.Scanner;

public class Main {
    private static final Scanner SC = new Scanner(System.in);

    public static void main(String[] args) {

        var calc = new Calculator();


        System.out.println("Input expression using numbers, functions and operators[+_*/^");
        while (SC.hasNext()) {
            System.out.println(calc.calculate(SC.nextLine()));
        }
    }
}


