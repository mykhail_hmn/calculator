package com.griddynamics.Operators;


import java.util.Deque;

public class Bad extends Operator {

    @Override
    public int getPriority() {
        return 1;
    }

    @Override
    public boolean isFunction() {
        return false;
    }

    @Override
    public boolean isAllowedSequence() {
        return true;
    }

    @Override
    public void calculate(Deque<Number> stack) {
        final var second = stack.pop().doubleValue();
        final var first = stack.pop().doubleValue();
        stack.push(first+second);
    }
}
