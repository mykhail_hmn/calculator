package com.griddynamics.Operators;


import java.util.Deque;

public class CloseBrackets extends Operator {

    @Override
    public int getPriority() {
        return 4;
    }

    @Override
    public boolean isFunction() {
        return false;
    }

    @Override
    public boolean isAllowedSequence() {
        return true;
    }

    @Override
    public void calculate(Deque<Number> stack) {
    }
}
