package com.griddynamics.Operators;



import java.util.Deque;
import java.util.HashMap;

public abstract class Operator {

    public static HashMap<String, Operator> operators = new HashMap<String, Operator>();
    public abstract int getPriority();
    public abstract boolean isFunction();
    public abstract boolean isAllowedSequence();

    public abstract void calculate(Deque<Number> stack) throws ArithmeticException;
}