package com.griddynamics.Operators;

import java.util.Deque;

public class Multiply extends Operator {

    @Override
    public int getPriority() {
        return 2;
    }

    @Override
    public boolean isFunction() {
        return false;
    }

    @Override
    public boolean isAllowedSequence() {
        return false;
    }

    @Override
    public void calculate(Deque<Number> stack) {
        final var second = stack.pop().doubleValue();
        final var first = stack.pop().doubleValue();
        stack.push(first * second);
    }
}
