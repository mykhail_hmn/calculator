package com.griddynamics.Operators;

import java.util.Deque;

public class Cosinus extends Operator {

    @Override
    public int getPriority() {
        return 3;
    }

    @Override
    public boolean isFunction() {
        return false;
    }

    @Override
    public boolean isAllowedSequence() {
        return false;
    }

    @Override
    public void calculate(Deque<Number> stack) {
        final var value = stack.pop().doubleValue();
        stack.push(Math.cos(value));
    }
}
