package com.griddynamics.Operators;

import java.util.Deque;

public class Divide extends Operator {

    @Override
    public int getPriority() {
        return 2;
    }

    @Override
    public boolean isFunction() {
        return false;
    }

    @Override
    public boolean isAllowedSequence() {
        return false;
    }

    @Override
    public void calculate(Deque<Number> stack) {
        final double second = stack.pop().doubleValue();
        final double first = stack.pop().doubleValue();
            try {
                stack.push(first / second);
                if (second == 0)
                    throw new ArithmeticException();
            }
             catch (ArithmeticException e){
                System.err.println(e);
            }


    }
}
