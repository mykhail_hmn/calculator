package com.griddynamics;

import com.griddynamics.Operators.*;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.regex.Pattern;

public class Calculator {
    private static final Pattern PATTERN = Pattern.compile("(?<=\\G(\\d+(?!\\d+)|\\w+(?!\\w+)|\\(|\\)|\\.|\\+|/|\\*|\\^|-))\\s*");

    // private static final Pattern SYMBOLS = Pattern.compile("(?<=(\\(?\\d)|\\w(?!\\w)|\\(|\\)|,\\.|\\+|\\*/|-|\\^)");
    private static final Pattern NUMBERS = Pattern.compile("\\d+");
    private final Deque<Operator> operations = new ArrayDeque<Operator>();
    private final Deque<Number> stack = new ArrayDeque<>();
    private Operator operation;
    private Operator previousOperation;
    private boolean number;
    private boolean floatVal;


    public Calculator() {

        Operator.operators.put("+", new Addition());
        Operator.operators.put("-", new Minus());
        Operator.operators.put("(", new OpenBrackets());
        Operator.operators.put(")", new CloseBrackets());
        Operator.operators.put("*", new Multiply());
        Operator.operators.put("/", new Divide());
        Operator.operators.put("sin", new Sinus());
        Operator.operators.put("cos", new Cosinus());
        Operator.operators.put("tg", new Tangens());
        Operator.operators.put("sqrt", new Sqrt());
        Operator.operators.put("pow", new Power());
        Operator.operators.put("^", new PowerX());

    }

    public Double calculate(String source) {
        String[] matches = PATTERN.split(source);
        var length = matches.length;

        previousOperation = null;
        floatVal = false;

        try {
            for (int i = 0; i < length; i++) {
                number = NUMBERS.matcher(matches[i]).matches();
                if (floatVal || number) {
                    numberConstant(matches[i]);
                } else if (".".equals(matches[i])) {
                    floatVal = true;
                } else if ((operation = Operator.operators.get(matches[i])) != null) {
                    doOperation((i + 1 < length) ? matches[i + 1] : null);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }


        while (operations.size() > 0) {
            execute(operations.pop());
        }


        return stack.pop().doubleValue();
    }

    private void doOperation(String next) throws MyException {
        if (previousOperation == null || operation.isFunction() ||
                (previousOperation == operation && operation.isAllowedSequence()
                        || previousOperation.isAllowedSequence() ^ operation.isAllowedSequence())) {
            if (operation.isFunction() && Operator.operators.get(next) != Operator.operators.get("("))
                throw new MyException("You need to place " + "( " + " after function!");
            insert(operation);
            previousOperation = operation;

        }
    }

    private void numberConstant(String m) throws MyException {
        if (floatVal) {
            final Number previous = stack.pop();
            if (previousOperation != null || !number || previous instanceof Double)
                throw new MyException("Can't create " + "Double" + " value");
            stack.push(Double.parseDouble("0." + m) + previous.doubleValue());
            floatVal = false;
        } else {
            stack.push(Integer.parseInt(m));
        }
        previousOperation = null;
    }

    private void execute(Operator operation)  {

        operation.calculate(stack);
    }


    private void insert(Operator operation) {
        if (operation == Operator.operators.get(")")) {
            while (operations.peek() != Operator.operators.get("(")) {
                execute(operations.pop());
            }
            operations.pop();
        } else {
            while (operations.size() > 0) {
                final Operator o = operations.peek();
                if (o.getPriority() >= operation.getPriority()
                        && !(o.isFunction() && operation == Operator.operators.get("("))
                        && o != Operator.operators.get("(")) {
                    operations.pop();
                    execute(o);
                } else {
                    break;
                }
            }
            operations.push(operation);
        }
    }

}
